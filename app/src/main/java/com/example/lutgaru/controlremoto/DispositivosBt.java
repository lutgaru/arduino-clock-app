package com.example.lutgaru.controlremoto;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.IdentityHashMap;
import java.util.Set;


public class DispositivosBt extends AppCompatActivity {


    private static final String TAG="DispositivosBT";

    ListView IdLista;

    public static String EXTRA_DEVICE_ADDRESS="device_address";

    private BluetoothAdapter mBtAdapter;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispositivos_bt);
    }

    public void onResume(){

        super.onResume();

        VerifivarEstadoBt();

        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.nombre_dispositivos);

        IdLista=(ListView) findViewById(R.id.Lista);
        IdLista.setAdapter(mPairedDevicesArrayAdapter);
        IdLista.setOnItemClickListener(mDeviceClickListener);

        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        Set<BluetoothDevice> pariedDevices = mBtAdapter.getBondedDevices();

        if(pariedDevices.size()>0){

            for (BluetoothDevice device: pariedDevices){

                mPairedDevicesArrayAdapter.add(device.getName() +"\n"+device.getAddress());

            }

        }

    }

    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView av, View v, int arg2, long arg3) {

            String info=((TextView) v).getText().toString();
            String address= info.substring(info.length() - 17);

            Intent i = new Intent(DispositivosBt.this, Control.class);//<-<- PARTE A MODIFICAR >->->
            i.putExtra(EXTRA_DEVICE_ADDRESS, address);
            startActivity(i);

        }
    };


    private void VerifivarEstadoBt(){

        mBtAdapter=BluetoothAdapter.getDefaultAdapter();
        if(mBtAdapter==null){
            Toast.makeText(getBaseContext(),"El Dispositivo no soporta Bluetooth", Toast.LENGTH_SHORT).show();
        }else {
            if(mBtAdapter.isEnabled()){
                Log.d(TAG,"...Bluetooth Activado...");
            }else{
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent,1);
            }
        }

    }

}
